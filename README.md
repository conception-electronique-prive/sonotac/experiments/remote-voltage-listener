# Sonotac remote voltage listener

Python script for listening voltage change on a remote

## Usage
`-p`, `--port` Serial port, mandatory  
`-f`, `--file` Output file, default to `output`