import datetime

from serial import Serial
import argparse
import re


class VoltageListener:
    def __init__(self, port, file):
        self.port = port
        self.file = file + ".csv"
        self.regex = r'I\s\(\d+\)\s\[BATTERY\]: Voltage: (\d+)\s\((\d+\.\d+)\)'

    def run(self):
        try:
            with Serial(port=self.port, baudrate=115200) as s:
                with open(self.file, mode="w") as f:
                    f.write("time,adc,voltage\n")

                while True:
                    line = s.readline().decode("utf-8")
                    print(line, end="")
                    findings = re.findall(self.regex, line)
                    for finding in findings:
                        with open(self.file, mode="a") as f:
                            f.write(f'{datetime.datetime.now()},{finding[0]},{finding[1]}\n')

        except KeyboardInterrupt as e:
            pass


def parse_args():
    parser = argparse.ArgumentParser();
    parser.add_argument("-p", "--port", type=str, required=True)
    parser.add_argument("-f", "--file", type=str, default="output")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    vl = VoltageListener(args.port, args.file)
    vl.run()
